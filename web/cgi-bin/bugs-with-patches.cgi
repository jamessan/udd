#!/usr/bin/ruby
# Used by bdmurray @ ubuntu

$:.unshift('../../rlibs')
require 'udd-db'

puts "Content-type: text/plain\n\n"

DB = Sequel.connect(UDD_GUEST)
DB["select distinct bugs.id, bugs.package, bugs.source from bugs
 where id in (select id from bugs_tags where tag='patch')
 and id not in (select id from bugs_usertags where email='ubuntu-devel@lists.ubuntu.com' and tag = 'ubuntu-patch')
and status != 'done'"].all.sym2str.each do |row|
  puts "#{row['source']},https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=#{row['id']},#{row['id']}"
end
