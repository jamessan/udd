#!/bin/sh -e
TARGETDIR=/srv/udd.debian.org/mirrors/
SUBDIR=machine-readable
rm -rf $TARGETDIR/${SUBDIR}
cd ${TARGETDIR}

# work around a regression in wget from wheezy to jessie
# see https://wiki.debian.org/ServicesSSL#wget
WGET=wget
dir=/etc/ssl/ca-debian
test -d $dir && WGET="wget --ca-directory=$dir"

$WGET -q http://blends.alioth.debian.org/machine-readable/machine-readable.tar.bz2 -O - | tar xj
